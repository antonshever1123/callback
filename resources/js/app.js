
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import Vue from 'vue'
import axios from 'axios';
import VueRouter from 'vue-router'

Vue.use(VueRouter)

axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest',
    'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
};

// project
import ProjectIndex  from './components/Project/Index';
import ProjectShow   from './components/Project/Show';
import ProjectCreate from './components/Project/Create';
import ProjectEdit   from './components/Project/Edit';

import Login    from './components/Login';
import Register from './components/Register';

import VuePagination from './components/Pagination';

Vue.component('vue-pagination', VuePagination)


Vue.component('form-checkbox', require('./components/Option/Checkbox'))
Vue.component('form-input', require('./components/Option/Input'))

Vue.component('loader', require('./components/Loader'))


var router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'index',
            component: ProjectIndex
        },{
            path: '/project/create',
            name: 'create',
            component: ProjectCreate
        },{
            path: '/project/:project_id/edit',
            name: 'edit',
            component: ProjectEdit,
            props: true
        },{
            path: '/project/:project_id',
            name: 'show',
            component: ProjectShow,
            props: true
        },{
            path: '/login',
            name: 'login',
            component: Login
        },{
            path: '/register',
            name: 'register',
            component: Register
        },
    ]
});

new Vue({
    el: '#app',
    router
});