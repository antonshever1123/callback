@extends('layouts.app')

@section('content')
  <project-edit :project_id="{{ $project->id }}"></project-edit>
@endsection
