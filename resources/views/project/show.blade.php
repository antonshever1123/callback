@extends('layouts.app')

@section('content')
    <project-show :project_id="{{ $project->id }}"></project-show>
@endsection