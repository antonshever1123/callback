<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use CallbackService;
use App\Callback;
use Auth;

class CallbackController extends Controller
{
    public function receiver(CallbackService $callbackService)
    {
        $callbackService->receiver();
    }
}