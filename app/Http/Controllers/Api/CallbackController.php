<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Auth;

use App\Callback;
use App\Project;

class CallbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Project $project)
    {
        $callbacks = $project->callbacks()->paginate(5);
        
        return response()->json($callbacks);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Callback $callback)
    {
        $result = [];
        $code = 200;

        // Check permission by creator
        if( $callback->project->user_id == Auth::id() ){
            $callback->delete();
        }else{
            $result['delete'] = "You do not have access.";
            $code = 403;
        }

        return response()->json($result, $code);
    }
}