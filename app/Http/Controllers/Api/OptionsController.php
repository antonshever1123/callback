<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use OptionsService;

use App\Project;


class OptionsController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function all(OptionsService $options)
    {
        return response()->json($options->all());
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function allWithData(OptionsService $options, Project $project)
    {
        $options->setValue(
            $project->setting->pluck('value', 'name')
        );

        return response()->json($options->all());
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function methods(OptionsService $options)
    {
        return response()->json($options->getMethods());
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function methodsWithData(OptionsService $options, Project $project)
    {
        $options->setValue(
            $project->setting->pluck('value', 'name')
        );

        return response()->json($options->getMethods());
    }
}