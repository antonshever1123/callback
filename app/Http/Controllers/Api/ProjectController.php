<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Auth;
use Validator;

use App\Project;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Auth::user()
            ->projects()->paginate(5);

        $projects->each(function (&$item) {
            $item->callbacks_count = $item->callbacks->count();
            $item->callbacks_sum = $item->callbacks->sum('counter');
        });

        return response()->json($projects);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'alias' => 'required|unique:projects|max:255',
        ]);

        $result = [];
        $code = 200;
        
        if ($validator->fails()) {
            $result = $validator->errors()->messages();
            $code = 422;
        }
        else {
            $project = Auth::user()->projects()->create($request->all());

            // TODO - create some checking, is option registered
            foreach ($request->input('options') as $option) {
                $project->setting()->create([
                    'name' => $option['name'],
                    'value' => $option['value']
                ]);
            }
            $result = $project;
        }
        return response()->json($result, $code);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        return response()->json($project);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Project $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'alias' => "required|max:255|unique:projects,alias,$project->id",
        ]);

        $result = [];
        $code = 200;

        if ($validator->fails()) {
            $result = $validator->errors()->messages();
            $code = 422;
        }
        else {
            $project->update($request->all());

            $options = $request->input('options');

            foreach ($options as $option) {
                $project->setting()->where('name', $option['name'])->update([
                    'name' => $option['name'],
                    'value' => $option['value']
                ]);
            }

            $result = $project;
        }

        return response()->json($result, $code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $result = [];
        $code = 200;

        // Check permission by creator
        if( $project->user_id == Auth::id() ){
            $project->delete();
        }else{
            $result['delete'] = "You do not have access.";
            $code = 403;
        }

        return response()->json($result, $code);
    }
}