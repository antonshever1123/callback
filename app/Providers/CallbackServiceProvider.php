<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Library\Callback;

use App\Project;

class CallbackServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Callback::class, function ($app){
            return new Callback(
                // get project by alias OR Fail
                Project::where('alias', $app->request->route('project_alias'))->firstOrFail()
            );
        });
    }
}
