<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'alias', 'user_id',
    ];

    public function callbacks()
    {
        return $this->hasMany('App\Callback', 'project_id')->orderBy('updated_at', 'desc');
    }

    public function setting()
    {
        return $this->hasMany('App\ProjectSetting', 'project_id');
    }
}
