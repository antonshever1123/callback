<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Callback extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'project_id', 'request', 'hash', 'counter', 'method'
    ];

    public function project()
    {
        return $this->hasOne('App\Project', 'id', 'project_id');
    }
}
