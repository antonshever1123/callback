<?php

namespace App\Library;

class Options {
    private $options = [];

    public function __construct() {

        // [][]['value'] it's by default

        $this->options = collect([
            'methods' => [
                [
                    'name' => 'post',
                    'type' => 'checkbox',
                    'value' => 0,
                ],
                [
                    'name' => 'get',
                    'type' => 'checkbox',
                    'value' => 1,
                ],
                [
                    'name' => 'put',
                    'type' => 'checkbox',
                    'value' => 0,
                ],
                [
                    'name' => 'patch',
                    'type' => 'checkbox',
                    'value' => 0,
                ],
                [
                    'name' => 'delete',
                    'type' => 'checkbox',
                    'value' => 0,
                ],
                [
                    'name' => 'head',
                    'type' => 'checkbox',
                    'value' => 0,
                ],
                [
                    'name' => 'options',
                    'type' => 'checkbox',
                    'value' => 0,
                ]
            ],
            'options' => [
                [
                    'name' => 'headers',
                    'type' => 'checkbox',
                    'value' => 0,
                ]
            ]
        ]);
    }

    public function getMethods() {
        return $this->options->get('methods');
    }

    public function getMethodsCollect() {
        return collect($this->options->get('methods') );
    }

    public function all() {
        return $this->options->flatten(1);
    }

    public function allCollect() {
        return $this->options->flatten(1);
    }

    public function getOptions() {
        return $this->options;
    }

    /**
     * @param $options Array. name => value
     */
    public function setValue( $optionsData )
    {
        $this->options = $this->options
            ->map(function ($type) use ($optionsData){
                foreach ($type as $key => $option) {
                    if (isset($optionsData[$option['name']])) {
                        $type[$key]['value'] = $optionsData[$option['name']];
                    }
                }
                return $type;
            });

        return $this;
    }
}