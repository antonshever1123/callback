<?php
namespace App\Library;

use Illuminate\Http\Request;
use OptionsService;

class Callback
{
    private $projectModel = null;
    private $data = [];

    public function __construct( $projectModel )
    {
        $this->projectModel = $projectModel;
    }

    public function receiver()
    {
        $request = app(Request::class);

        // check whitch method is active, and set data
        $this->_getDataFromRequest($request);

        // save data
        $this->_save($request->method());
    }

    private function _getDataFromRequest( $request )
    {
        $setting = $this->projectModel->setting->pluck('value', 'name'); 

        $request_method = strtolower($request->method());

        // get methods
        $methods = app(OptionsService::class)->getMethodsCollect()->pluck('name')->all();
//dd($methods);
        // if all the methods are disable, go exit;
        if(! in_array( $request_method, $methods ) ){exit;}


        // if the method 'headers' is active
        if( isset( $setting['headers'] ) && $setting['headers'] ){
            $this->data['headers'] = $request->headers->all();
        }

        $this->data['data'] = $request->all();
    }

    private function _save( $method )
    {
        $this->data = json_encode($this->data);

        // get callback by hash
        $callbackModel = $this->projectModel->callbacks()->firstOrNew([
            'hash' => md5($this->data)
        ]);

        // update callback
        $callbackModel->request = $this->data;
        $callbackModel->counter += 1;
        $callbackModel->method  = $method;
        $callbackModel->save();
    }
}