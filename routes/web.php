<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware('auth')->group(function() {

    Route::get('/', 'ProjectController@index');


    Route::resource('project', 'ProjectController', ['except' => ['index', 'store', 'update', 'destroy']]);
});

Route::any('/callback/{project_alias}', 'CallbackController@receiver')->where('project_alias', '.*');



Route::middleware(['auth', 'ajax'])->prefix('api')->group(function() {

	Route::resource('projects', 'Api\ProjectController', ['except' => ['edit', 'create']]);

    Route::resource('callbacks', 'Api\CallbackController', ['only' => ['destroy']]);
	Route::get('callbacks/{project}', 'Api\CallbackController@index')->where('project', '[0-9]+');

	Route::get('options/all', 'Api\OptionsController@all');
	Route::get('options/all/{project}', 'Api\OptionsController@allWithData')->where('project', '[0-9]+');
	// Route::get('options/methods', 'Api\OptionsController@methods');
	// Route::get('options/methods/{project}', 'Api\OptionsController@methodsWithData')->where('project', '[0-9]+');
});